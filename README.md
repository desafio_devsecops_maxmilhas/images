# images

Repositorio para manter as images para build de pacotes

As imagens devem ser abstratas evitando codigos repetitivos que dificultam manter e dar manutençao na soluçao implementada.


## ansible
A imagem do ansible serve de base para qualquer solucao que for implementada, os pacotes instalados sao:

- ansible
- epel-release
- delta-rpm
- git

Toda solucao deve ser implementada via playbook e utilizar o git como fluxo de aprovaçao de mudanças, 
dessa forma toda solicitaçao/ aprovaçao sera auditada e podera ser consultada a qualquer momento via git. O processo de auditoria,
atende as soluçoes on-premises, cloud privada e/ou cloud publica.

O comando para gerar o build do ansible manualmente e:
<code>docker build --no-cache -t ansiblemaxmilhas -f ansible .</code>

<b>O comando devera ser executado dentro da pasta images</b>

As versoes para os pacotes foram fixadas, com o objetivo de obter hardenizaçao, um melhor controle da infraestrutura e testes mais eficazes.